d3.select(".icon").append('text')
    .attr('text-anchor', 'middle')
    .attr('dominant-baseline', 'central')
    .attr('font-family', 'FontAwesome')
    .attr('fill', 'white')
    .attr('transform', 'translate(40, 50)')
    .attr("class", "fa")
    .attr('font-size', '40px')
    .text(function (d) { return "\uf005"; });



d3.select("button").on("click", () => {
    console.log("Clicked");
    var svg = document.querySelector('svg');
    var xml = new XMLSerializer().serializeToString(svg);
    var svg64 = btoa(unescape(encodeURIComponent(xml))); //for utf8: btoa(unescape(encodeURIComponent(xml)))
    var b64start = 'data:image/svg+xml;base64,';
    var image64 = b64start + svg64;
    // return image64;
    console.log(image64);

    var canvas = document.querySelector("canvas"),
        context = canvas.getContext("2d");

    var image = new Image;
    image.src = image64;
    image.onload = function () {
        context.drawImage(image, 0, 0);
        context.font='FontAwesome';
        var a = document.createElement("a");
        a.download = "fallback.png";
        a.href = canvas.toDataURL("image/png");
        a.click();
    }
})

d3.select(".icon_container")
    .attr("fill", "white")
    .attr("width", "100")
    .attr("height", "100")